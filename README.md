# Coding Challenge
Coding challenge for Latitude Financial.

## Problem
Suppose we could access yesterday's stock prices as a list, where:

- The indices are the time in minutes past trade opening time, which was 10:00am local time.
- The values are the price in dollars of the stock at that time.
- So if the stock cost $5 at 11:00am, `stock_prices_yesterday[60] = 5`

Write an efficient function that takes an array of stock prices and returns the best profit could have been made from 1 purchase and 1 sale of 1 stock.

#### For example:
```
int[] stockPrices = {10, 7, 5, 8, 11, 9};

Assert.assertEquals (6, getMaxProfit(stockPrices)); // returns 6 (buy at $5 sell at $11)
```
You must buy before you sell. You may not buy and sell in the same time step (at least 1 minute must pass).

#### Expectations

- Implement a solution in Java.
- Prove it works by creating unit tests that test the possible scenarios that the numbers could present.
- Include any comments that you think will be relevant to provide any context around the approach taken / solution developed.
- We prefer the response as a Git repo or ZIP File.
- Once you have completed the challenge it will be reviewed and next steps discussed. If you are unsuccessful, you will receive feedback as to why


## Solution
This solution is based on:
- Java 8
- JUnit
- Mockito

Spring boot framework is used to add support for spring and tests related libraries.

Note: Spring support wasn't really required for this solution. It is only kept for future use, if any.

### User story:
As a stock holder I want to know the maximum profit which I could have made on a buy trade followed by a sell trade
on a single stock
###### Use case:
Possible use case for this problem is for reporting the best profit based on historical prices

#### Conditions:
1. Buying time should always be before selling time
2. Only one buy trade allowed
3. Only one sell trade allowed

#### Assumptions:
1. Solution returns 0 in below cases <br>
a. Prices array null <br>
b. Prices array length 0 <br>
2. Negative stock prices are not considered

### Implementation
The solution is implemented in class `HistoricalSale`.
Problem is solved using two algorithms
- Streams (see method `getMaxProfitStream`)
- For loop (see method `getMaxProfitLoop`)

Tests are included in class `HistoricalSaleTest`
