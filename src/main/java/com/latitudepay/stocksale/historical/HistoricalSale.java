package com.latitudepay.stocksale.historical;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * <h1>HistoricalSale</h1>
 * <i>Possible use case for this problem is for reporting the best profit based on historical prices</i>
 * <br>
 * <h2>User story:</h2>
 * As a stock holder I want to know the maximum profit which I could have made on a buy trade followed by a sell trade
 * on a single stock
 * <br>
 * <h3>Conditions:</h3>
 * 1. Buying time should always be before selling time <br>
 * 2. Only one buy trade allowed <br>
 * 3. Only one sell trade allowed
 * <br>
 * <h3>Assumptions:</h3>
 * 1. Solution returns 0 in below cases <br>
 * a. Prices array null <br>
 * b. Prices array length 0 <br>
 * 2. Negative stock prices are not considered
 */
@Component
public class HistoricalSale {

    public int getMaxProfitLoop(int[] stockPrices) {
        if (isValidData(stockPrices)) {
            return 0;
        }

        // start with first stock price
        int minSoFar = stockPrices[0];
        int maxSoFar = stockPrices[0];

        // maxProfit is zero at the start
        int maxProfit = 0;

        for (int currentPrice : stockPrices) {
            // ignore negative prices
            if (currentPrice <= 0) {
                continue;
            }

            // case of increase in stock price,
            // 1. calculate current profit
            // 2. compare with previous max profit
            // 3. assign the bigger value to max profit
            if (currentPrice > maxSoFar) {
                maxSoFar = currentPrice;
                int currentProfit = maxSoFar - minSoFar;

                maxProfit = Math.max(currentProfit, maxProfit);
            }
            // case of decrease in stock price,
            // 1. reset minSoFar and maxSoFar
            else if (currentPrice < minSoFar) {
                minSoFar = currentPrice;
                maxSoFar = currentPrice;
            }
        }

        return maxProfit;
    }

    public int getMaxProfitStream(int[] stockPrices) {
        // validate stockPrices
        if (isValidData(stockPrices)) {
            return 0;
        }

        // find min and max and their respective indexes
        int min = Arrays.stream(stockPrices).min().orElse(0);
        int minIndex = IntStream.range(0, stockPrices.length)
                .filter(index -> stockPrices[index] == min).findFirst().orElse(0);

        int max = Arrays.stream(stockPrices).max().orElse(0);
        int maxIndex = IntStream.range(0, stockPrices.length)
                .filter(index -> stockPrices[index] == max).findFirst().orElse(0);

        // case of most profitable trade
        // return maxProfit (max - min)
        if (minIndex < maxIndex) {
            return max - min;
        }
        // for all other cases,
        // start over with a subset of stock prices
        else {
            int[] sliceData = Arrays.stream(stockPrices, minIndex, stockPrices.length - 1).toArray();

            return getMaxProfitStream(sliceData);
        }
    }

    private boolean isValidData(int[] data) {
        return data == null || data.length == 0;
    }

}
