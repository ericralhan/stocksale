package com.latitudepay.stocksale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StocksaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(StocksaleApplication.class, args);
	}

}
