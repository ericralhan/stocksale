package com.latitudepay.stocksale.historical;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HistoricalSaleTest {

    private final HistoricalSale historicalSale = new HistoricalSale();

    @ParameterizedTest
    @MethodSource({
            "exampleScenario",
            "singleOrNoStockPrice",
            "twoStockPrices",
            "risingStockPrices",
            "fallingStockPrices",
            "mixedStockPrices"
    })
    public void getMaxProfit_testLoopAlgorithm(String scenarioName, int[] stockPrices, int expected) {
        int answer = historicalSale.getMaxProfitLoop(stockPrices);

        assertEquals(expected, answer, scenarioName);
    }

    @ParameterizedTest
    @MethodSource({
            "exampleScenario",
            "singleOrNoStockPrice",
            "twoStockPrices",
            "risingStockPrices",
            "fallingStockPrices",
            "mixedStockPrices"
    })
    public void getMaxProfit_testStreamAlgorithm(String scenarioName, int[] stockPrices, int expected) {
        int answer = historicalSale.getMaxProfitStream(stockPrices);

        assertEquals(expected, answer, scenarioName);
    }

    /******* Test setup *******/
    private static Stream<Arguments> exampleScenario() {
        return Stream.of(
                Arguments.of("Scenario:: example stockPrices", new int[]{10, 7, 5, 8, 11, 9}, 6)
        );
    }

    private static Stream<Arguments> singleOrNoStockPrice() {
        return Stream.of(
                Arguments.of("Scenario:: Null stockPrices", null, 0),
                Arguments.of("Scenario:: Empty stockPrices", new int[]{}, 0),
                Arguments.of("Scenario:: Zero stockPrice", new int[]{0}, 0),
                Arguments.of("Scenario:: Random stockPrice", new int[]{new Random().nextInt()}, 0),
                Arguments.of("Scenario:: Integer.MAX_VALUE", new int[]{Integer.MAX_VALUE}, 0)
        );
    }

    private static Stream<Arguments> twoStockPrices() {
        return Stream.of(
                Arguments.of("Scenario:: 0, Integer.MAX_VALUE", new int[]{0, Integer.MAX_VALUE}, 2147483647),
                Arguments.of("Scenario:: Integer.MAX_VALUE, 0", new int[]{Integer.MAX_VALUE, 0}, 0),
                Arguments.of("Scenario:: 0, 0", new int[]{0, 0}, 0),
                Arguments.of("Scenario:: 0, 1", new int[]{0, 1}, 1),
                Arguments.of("Scenario:: 1, 0", new int[]{1, 0}, 0),
                Arguments.of("Scenario:: 1, 1", new int[]{1, 1}, 0),
                Arguments.of("Scenario:: 100, 75", new int[]{100, 75}, 0),
                Arguments.of("Scenario:: 25, 50", new int[]{25, 50}, 25)
        );
    }

    private static Stream<Arguments> risingStockPrices() {
        return Stream.of(
                Arguments.of("Scenario:: 1 to 20", IntStream.rangeClosed(1, 20).toArray(), 19),
                Arguments.of("Scenario:: 30 to 50", IntStream.rangeClosed(30, 50).toArray(), 20),
                Arguments.of("Scenario:: 80 onwards even",
                        IntStream.iterate(80, n -> n + 2).limit(10).toArray(), 18),
                Arguments.of("Scenario:: 81 onwards odd",
                        IntStream.iterate(81, n -> n + 2).limit(10).toArray(), 18)
        );
    }

    private static Stream<Arguments> fallingStockPrices() {
        return Stream.of(
                Arguments.of("Scenario:: 80 onwards even falling",
                        IntStream.iterate(80, n -> n - 2).limit(10).toArray(), 0),
                Arguments.of("Scenario:: 45 onwards odd falling",
                        IntStream.iterate(45, n -> n - 2).limit(10).toArray(), 0),
                Arguments.of("Scenario:: 10 onwards falling",
                        IntStream.iterate(10, n -> n - 1).limit(10).toArray(), 0),
                Arguments.of("Scenario:: 100 onwards falling",
                        IntStream.iterate(100, n -> n - 1).limit(10).toArray(), 0)
        );
    }

    private static Stream<Arguments> mixedStockPrices() {
        return Stream.of(
                Arguments.of(
                        "Scenario:: 1 mixed", IntStream.concat(
                                IntStream.iterate(1, n -> n + 1).limit(10),
                                IntStream.iterate(11, n -> n - 1).limit(10)
                        ).toArray(), 10
                ),
                Arguments.of(
                        "Scenario:: 2 mixed", IntStream.concat(
                                IntStream.iterate(100, n -> n - 1).limit(10),
                                IntStream.iterate(100, n -> n + 1).limit(10)
                        ).toArray(), 18
                ),
                Arguments.of(
                        "Scenario:: 3 mixed", IntStream.concat(
                                IntStream.iterate(10, n -> n + 1).limit(20),
                                IntStream.iterate(20, n -> n + 1).limit(20)
                        ).toArray(), 29
                )
        );
    }

}
